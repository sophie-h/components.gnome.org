use std::ops::Deref;

use askama::Template;
use page_tools::{Components, GnomeCategory, People, Site};

// used in templates
use crate::filters;

#[derive(Template)]
#[template(path = "index.html")]
pub struct Index<'a> {
    pub site: &'a Site,
    pub components: &'a Components,
    pub people: &'a People,
}
