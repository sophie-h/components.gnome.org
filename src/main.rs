mod cli;
mod filters;
mod generate;
mod page;

fn main() {
    cli::main();
}
