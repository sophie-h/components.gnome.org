use page_tools::output::{self, Dir, Output, OutputSimple, Scss};
use page_tools::{Components, Language, People, Site};

use crate::page;

pub fn generate() -> color_eyre::Result<()> {
    output::prepare();

    let mut components = Components::from_upstream_cached().unwrap();
    let people = People::from_upstream_cached().unwrap();

    components.components.sort_by_cached_key(|_, x| {
        (
            x.avatar_url.is_none(),
            x.name
                .clone()
                .unwrap_or_else(|| x.id.to_string())
                .to_lowercase(),
        )
    });

    Dir::from("assets/").output("assets/");

    Scss::from("scss/main.scss").output("assets/style/main.css");

    let site = Site::new(&Language::en());

    let overview = page::Index {
        site: &site,
        components: &components,
        people: &people,
    };
    overview.output("index.html");

    output::complete()?;

    Ok(())
}
