//! Filters available in templates
pub use page_tools::filters::*;

pub fn initials(input: &str) -> askama::Result<String> {
    let (x, y) = initials_tuple(input);

    Ok(format!("{x}{y}"))
}

fn initials_tuple(input: &str) -> (char, char) {
    let s = input.strip_prefix("lib").unwrap_or(input);

    let prefixes = s
        .split_once(' ')
        .or_else(|| s.split_once('-'))
        .map(|(x, y)| (x.chars().next(), y.chars().next()))
        .unwrap_or_else(|| {
            let mut chars = s.chars();
            (chars.next(), chars.next())
        });

    (
        prefixes.0.unwrap_or('?').to_uppercase().next().unwrap(),
        prefixes.1.unwrap_or('?').to_uppercase().next().unwrap(),
    )
}

pub fn initials_color(input: &str) -> askama::Result<String> {
    let n = input.as_bytes().iter().map(|x| *x as u64).sum::<u64>();

    let color = match n % 8 {
        0 => "blue",
        1 => "green",
        2 => "yellow",
        3 => "orange",
        4 => "red",
        5 => "purple",
        6 => "brown",
        7 => "dark",
        _ => unreachable!(),
    };

    let variant = match n % 3 {
        0 => 3,
        1 => 4,
        2 => 5,
        _ => unreachable!(),
    };

    Ok(format!("{color}{variant}"))
}
