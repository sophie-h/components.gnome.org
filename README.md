# GNOME Components

- https://developer.gnome.org/components/

This page lists all offical GNOME software and all Circle components that are not apps.

The data is aggregated from the projects [.doap-files](https://gitlab.gnome.org/Teams/Websites/apps.gnome.org/-/blob/main/METADATA.md#doap). The icon is the project avatar (organization avatar on GitHub).

Project icons should be generated with the [Emblem app](https://apps.gnome.org/en/Emblem/) as PNG using the "Square" option. They can be either set using the GitLab options or by adding a "logo.png" to the projects root.

If you find errors or out-of-date metadata, please report these to the respective projects.

## Building the Page

The page can be built like a Flatpak app. Use the ▶ (Run Project) button in [Builder](https://apps.gnome.org/Builder/). After the build completes, the page is available under `http://localhost:8000/` until the ■ (Stop Running Project) button is pressed.
